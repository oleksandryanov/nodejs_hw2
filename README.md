# NoteManager Backend

## Overview

NoteManager is a backend application built with Express.js and Node.js for managing notes. It utilizes MongoDB as the database with Mongoose as the library for database connection. This project incorporates JWT authorization for secure access to note management functionalities.

## Features

-   **RESTful API**: Well-defined and documented RESTful API endpoints for creating, reading, updating, and deleting notes.
-   **MongoDB Integration**: Integration with MongoDB for efficient storage and retrieval of notes.
-   **Mongoose Library**: Utilizes Mongoose as the library for connecting to MongoDB, providing schema-based solutions and data validation.
-   **JWT Authorization**: Secure authorization using JSON Web Tokens (JWT) to authenticate and authorize users for note management operations.
-   **Error Handling Middleware**: Implement error handling middleware to ensure proper handling of errors and graceful degradation.
-   **Scalability**: Designed with scalability in mind to handle a growing number of users and notes.

## Installation

1. Clone the repository: `git clone https://gitlab.com/oleksandryanov/note-managing-app-backend.git`
2. Navigate to the project directory: `cd note-managing-app-backend`
3. Install dependencies: `npm install`
4. Set up environment variables: Create a `.env` file in the root directory and define the following variables: [TEMPLATE](.env.template)
5. Start the server: `npm start`

## API Endpoints

-   **POST /api/auth/register**: Register a new user.
-   **POST /api/auth/login**: Login and receive a JWT token.
-   **GET /api/me**: Get user information.
-   **PATCH /api/me**: Edit user information.
-   **DELETE /api/me**: Delete user.
-   **GET /api/notes**: Get all notes.
-   **GET /api/notes**: Get all notes.
-   **GET /api/notes/:id**: Get a specific note by ID.
-   **POST /api/notes**: Create a new note.
-   **PUT /api/notes/:id**: Update a note by ID.
-   **PATCH /api/notes/:id**: Update the text in a note by it's ID.
-   **DELETE /api/notes/:id**: Delete a note by ID.

## Contributing

Contributions are welcome! If you have any suggestions, bug fixes, or new features to add, please feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or feedback, please contact me via email: oleksandr.yanov.eu@gmail.com.
