const CustomError = require('../CustomError');

const customErrorFunc = (err, req, res, next) => {
  if (err instanceof CustomError) {
    return res.status(err.status).json({ message: err.message });
  }
  console.log(next);
  return res.status(500).json({ message: 'Internal server error' });
};

module.exports = customErrorFunc;
