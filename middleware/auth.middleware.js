const jwt = require('jsonwebtoken');

const authFunc = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1]; // BEARER `token`

    if (!token) {
      res.status(401).json({ message: 'Not authorized' });
    }

    const decodedUser = jwt.verify(token, process.env.SECRET_KEY);

    req.user = decodedUser;
    next();
  } catch (e) {
    res.status(401).json({ message: 'Not authorized' });
  }
};

module.exports = authFunc;
