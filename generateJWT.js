const jwt = require('jsonwebtoken');

const generateJWT = (username, id) => jwt.sign(
  { username, id },
  process.env.SECRET_KEY,
  { expiresIn: '24h' },
);

module.exports = generateJWT;
