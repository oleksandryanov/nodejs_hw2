const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

require('dotenv').config();

const errorHandlingMiddleware = require('./middleware/error.middlware');
const appRoutes = require('./routers/index');

const accessedLog = fs.createWriteStream(path.join(__dirname, 'requestLogs.log'), { flags: 'a' });

// APP
const app = express();

app.use(cors());
app.use(express.json());
app.use(morgan('tiny', { stream: accessedLog }));

// ROUTES

app.use('/api', appRoutes);

// MIDDLEWARE

app.use(errorHandlingMiddleware);

const {
  PORT = 8080, DB_USER, DB_PASSWORD, DB_NAME,
} = process.env;

async function start() {
  try {
    await mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@${DB_NAME}.1iydlp0.mongodb.net/?retryWrites=true&w=majority`);

    app.listen(PORT, () => console.log(`Server started at port: ${PORT}`));
  } catch (err) {
    console.log(err);
  }
}

start();
