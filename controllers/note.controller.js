const Note = require('../models/Note');
const CustomError = require('../CustomError');

const getNotes = async (req, res, next) => {
  try {
    const { offset = 0, limit = 0 } = req.query;

    const notes = await Note.find({ userId: req.user.id }).skip(offset).limit(limit);

    return res.status(200).json({
      offset, limit, count: notes.length, notes,
    });
  } catch (err) {
    return next(CustomError.internalError(err.message));
  }
};

const getNote = async (res, req, next) => {
  try {
    const _id = req.params.id;

    if (!_id) {
      return next(CustomError.badRequest('id is not provided'));
    }

    const note = await Note.findOne({ _id });

    if (!note) {
      return next(CustomError.badRequest('There is no note with such id'));
    }

    if (note.userId !== req.user.id) {
      return next(CustomError.forbidden('Forbidden'));
    }

    return res.status(200).json({ note });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

const createNote = async (req, res, next) => {
  try {
    const { text } = req.body;

    if (!text) {
      return next(CustomError.badRequest('Text for note is not provided'));
    }

    const newNote = new Note({
      text,
      userId: req.user.id,
    });

    await newNote.save();

    return res.status(200).json({
      id: newNote.id,
      text: newNote.text,
      completed: newNote.completed,
      createdAt: newNote.createdAt,
      message: 'Success',
    });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

const editNote = async (req, res, next) => {
  try {
    const _id = req.params.id;
    const { text } = req.body;

    if (!_id) {
      return next(CustomError.badRequest('id is not provided'));
    }

    const note = await Note.findOne({ _id });

    if (!note) {
      return next(CustomError.badRequest('There is no note with such id'));
    }

    if (note.userId !== req.user.id) {
      return next(CustomError.forbidden('Forbidden'));
    }

    const newNote = await Note.findByIdAndUpdate(_id, { text });

    return res.status(200).json({
      text: newNote.text,
      completed: newNote.completed,
      createdAt: newNote.createdAt,
      updatedAt: newNote.updatedAt,
      message: 'Success',
    });
  } catch (err) {
    return next(CustomError.internalError(err.message));
  }
};

const editNoteText = async (req, res, next) => {
  try {
    const _id = req.params.id;

    if (!_id) {
      return next(CustomError.badRequest('id is not provided'));
    }

    const note = await Note.findOne({ _id });

    if (!note) {
      return next(CustomError.badRequest('There is no note with such id'));
    }

    if (note.userId !== req.user.id) {
      return next(CustomError.forbidden('Forbidden'));
    }

    await Note.findByIdAndUpdate(_id, { completed: !note.completed });

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

const deleteNote = async (req, res, next) => {
  try {
    const _id = req.params.id;

    if (!_id) {
      return next(CustomError.badRequest('id is not provided'));
    }

    const note = await Note.findOne({ _id });

    if (!note) {
      return next(CustomError.badRequest('There is no note with such id'));
    }

    if (note.userId !== req.user.id) {
      return next(CustomError.forbidden('Forbidden'));
    }

    await Note.findByIdAndDelete(_id);

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

module.exports = {
  getNotes,
  createNote,
  editNote,
  editNoteText,
  deleteNote,
  getNote,
};
