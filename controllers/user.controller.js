const bcrypt = require('bcryptjs');

const User = require('../models/User');
const CustomError = require('../CustomError');

const getUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user.id });

    if (!user) {
      return next(CustomError.unauthorized('Unauthorized'));
    }

    return res.status(200).json({
      user: {
        id: user.id,
        username: user.username,
        createdAt: user.createdAt,
      },
    });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

const editUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user.id });

    if (!user) {
      return next(CustomError.unauthorized('Unauthorized'));
    }

    const { oldPassword, newPassword } = req.body;

    const isPasswordCorrect = await bcrypt.compare(oldPassword, user.password);

    if (!isPasswordCorrect) {
      return next(CustomError.badRequest('Password is incorrect'));
    }

    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await User.findByIdAndUpdate(user.id, { password: hashedPassword });

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user.id });

    if (!user) {
      return next(CustomError.unauthorized('Unauthorized'));
    }

    await User.findOneAndDelete({ _id: user.id });

    return res.status(200).json({ message: 'success' });
  } catch (err) {
    return next(CustomError.internalError('Server error'));
  }
};

module.exports = {
  getUser,
  editUser,
  deleteUser,
};
