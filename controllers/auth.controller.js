const bcrypt = require('bcryptjs');

const generateJWT = require('../generateJWT');
const User = require('../models/User');
const ApiError = require('../CustomError');

const register = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      return next(ApiError.badRequest('Username or password is not provided'));
    }

    const isUser = await User.findOne({ username });

    if (isUser) {
      return next(ApiError.badRequest('Username is already taken'));
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = new User({
      username,
      password: hashedPassword,
    });

    await newUser.save();

    const token = generateJWT(username, newUser.id);

    res.status(200).json({ message: 'Success', token });
  } catch (err) {
    console.log(err);
    return next(ApiError.internalError('Server error'));
  }

  return null;
};

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      return next(ApiError.badRequest('Username or password is not provided'));
    }

    const user = await User.findOne({ username });

    if (!user) {
      return next(ApiError.badRequest('Username or password is incorrect'));
    }

    const isPasswordCorrect = await bcrypt.compare(password, user.password);

    if (!isPasswordCorrect) {
      return next(ApiError.badRequest('Username or password is incorrect'));
    }

    const token = generateJWT(user.username, user.id);

    res.status(200).json({ message: 'Success', jwt_token: token });
  } catch (err) {
    return next(ApiError.internalError('Server error'));
  }

  return null;
};

module.exports = {
  register,
  login,
};
