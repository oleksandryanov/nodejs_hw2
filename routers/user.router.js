const express = require('express');

const router = express.Router();

const userController = require('../controllers/user.controller');
const authMiddleware = require('../middleware/auth.middleware');

router.get('/me', userController.getUser);
router.patch('/me', authMiddleware, userController.editUser);
router.delete('/me', userController.deleteUser);

module.exports = router;
