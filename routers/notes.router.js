const express = require('express');

const router = express.Router();
const noteController = require('../controllers/note.controller');

router.get('/', noteController.getNotes);
router.post('/', noteController.createNote);
router.get('/:id', noteController.getNote);
router.put('/:id', noteController.editNote);
router.patch('/:id', noteController.editNoteText);
router.delete('/:id', noteController.deleteNote);

module.exports = router;
