const express = require('express');

const router = express.Router();

const authRouter = require('./auth.router');
const userRouter = require('./user.router');
const notesRouter = require('./notes.router');
const authMiddleware = require('../middleware/auth.middleware');

router.use('/auth', authRouter);
router.use('/users', authMiddleware, userRouter);
router.use('/notes', authMiddleware, notesRouter);

module.exports = router;
