const { Schema, model } = require('mongoose');

const NoteSchema = new Schema({
  userId: { type: Number, requiered: true },
  completed: { type: Boolean, default: false, required: false },
  text: { type: String, required: true },
  createdDate: { type: Date, default: Date.now, required: false },
}, { timestamps: true });

module.exports = model('Note', NoteSchema);
