const { Schema, model } = require('mongoose');

const UserSchema = new Schema({
  username: { type: String, requiered: true, unique: true },
  password: { type: String, required: true },
  createdDate: { type: Date, default: Date.now, required: false },
}, { timestamps: true });

module.exports = model('User', UserSchema);
