class CustomError extends Error {
  constructor(message, status) {
    super();
    this.message = message;
    this.status = status;
  }

  static badRequest(message) {
    return new CustomError(message, 400);
  }

  static unauthorized(message) {
    return new CustomError(message, 401);
  }

  static forbidden(message) {
    return new CustomError(message, 403);
  }

  static internalError(message) {
    return new CustomError(message, 500);
  }

  static badGateway(message) {
    return new CustomError(message, 502);
  }
}

module.exports = CustomError;
